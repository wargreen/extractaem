Ce script extrait les heures travaillées depuis les pdf des AEM pour les Intermittents du Spectacle et affiche les infos utiles pour l'actualisation pole emploi des dates effectuées depuis le début du statut en cours.
Il inclut ces informations dans un CSV pour permettre leurs traitement dans un tableur.

Notes d'usages disponibles en en-tête du fichier.

EXCEPTION :
Pour le GUSO, utiliser les feuillets de déclaration

USAGE :
1) Placer les AEM dans des dossiers de type `./$ANNEE/$MOIS`
2) Remplacer la date de renouvellement du statut en cours en ligne 3
3) Executer ce script depuis le dossier parent a ceux-ci  
