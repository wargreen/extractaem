#!/bin/bash

declare -r debut_statut=12/02/24

################################################################################
# Ce script extrait les heures travaillées depuis les pdf des AEM pour les     #
# Intermittents du Spectacle et affiche les infos utiles pour l'actualisation  #
# pole emploi des dates effectuées depuis le début du statut en cours.         #
# Il inclut ces informations dans un CSV pour permettre leurs traitement       #
# dans un tableur.                                                             #
#                                                                              #
# EXCEPTION :                                                                  #
# Pour le GUSO, utiliser les feuillets de déclaration                          #
#                                                                              #
# USAGE :                                                                      #
# 1) Placer les AEM dans des dossiers de type ./$ANNEE/$MOIS                   #
# 2) Remplacer la date de renouvellement du statut en cours en ligne 3         #
# 3) Executer ce script depuis le dossier parent a ceux-ci                     #
#                                                                              #
# DEPENDANCES :                                                                #
# pdftotext                                                                    #
#                                                                              #
# TODO :                                                                       #
# Gérer les arguments pour pouvoir calculer à partir d'une date anniversaire   #
#                                                                              #
# Pour toutes galères ou améliorations : Wargreen (wargreen@lebib.org)         #
################################################################################


#    Copyright (C) 2021-2023  Wargreen
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


declare -r DEBUG=0

declare -r ERR='\033[0;31m'
declare -r NC='\033[0m'

base=$(pwd)

declare -A date
declare -a nbr_aem_trait
declare -a nbr_aem_compt
declare -a nbr_non_trait=0
declare -a heuresTot=0
declare -a cachetsTot=0


## Logique pour les différent générateurs d'AEMs

# Test des générateurs
generateurPDF () {
    if grep -q -o -P 'sPAIEctacle' "$txt";
        then
            echo "spaiectacle";
            return
    fi
    
    if grep -q -o -P 'COGILOG' "$txt";
        then
            echo "cogilog"
            return
    fi
    
    if grep -q -o -P '83/GBUS02' "$txt";
        then
            echo "guso_aem"
            return
    fi
    
    ## Guso jusqu'a 2024
    if grep -q -o -P 'WEB' "$txt";
        then
            echo "guso_decla"
            return
    fi
    
    ## Guso depuis 2024
    if grep -q -o -P 'Déclaration unique et simplifiée' "$txt";
        then
            echo "guso24_decla"
            return
    fi
    
    if grep -q -o -P 'COOLPAIE' "$txt";
        then
            echo "coolpaie"
            return
    fi
    echo "inconnu"
}

# Implementation des différent générateurs

extract_spaiectacle () {
    if [ "${DEBUG}" -ne 0 ]
        then
        echo "Généré avec spaiectacles"
    fi
    dates=($(grep -o -P '((\d{2} ){2}\d{4})' "$txt" | sed 's/ /\//g'))
    date[debut]=${dates[1]}
    date[fin]=${dates[2]}
    date[heures]=$(grep -z -o -P '((\d{2} ){2}\d{4})\n.?\n\n(\d{1,2})\n' "$txt" | tr '\0' '\n' | tail -n2)
    date[employ]=$(grep -z -o -P '(\d{14}\n\d{4}?\n)\K.+' "$txt" | tr '\0' '\n' | tail -n2)
    date[brut]=$(grep -z -o -P '\n\d+\n\n\d+,\d+\n\n(\d+,\d+)' "$txt" | tr '\0' '\n' | tail -n2 | tr -d [:cntrl:] )
}

extract_cogilog () {
    if [ "${DEBUG}" -ne 0 ]
        then
        echo "Généré avec cogilog"
    fi
    date[debut]=$(grep -zoP '(\d{2}\n\d{2})\n' "$txt" | tr '\n' ' ' | tr -d [:cntrl:] | sed 's/ /\//g')
    date[debut]+=$(grep -zoP '(?<=embauche\n/ )(\d{4})\n' "$txt" | tr -d [:cntrl:] )
    date[fin]=$(grep -zoP '\d{2} \d{2}\n/ \d{4}\n(?=/CONTRIBUTIONS)' "$txt" | tr -d '\\n/' | tr -d [:cntrl:] | sed 's/ /\//g')
    date[heures]=$(grep -zoP '(?<=ET/OU\n\n)\d{1,3}(?=\n\nNon)' "$txt" | tr -d [:cntrl:] )
    date[employ]=$(grep -zoP '(?<=Raison Sociale ).*' "$txt" | tr -d [:cntrl:])
}

#extract_guso_aem () {
#    echo "Généré avec guso"
#    date[debut]=$(grep -zoP '(?<=Du )(\d{2}/\d{2}/\d{4})' "$txt" | tr -d [:cntrl:])
#    date[fin]=$(grep -zoP '(?<=au )(\d{2}/\d{2}/\d{4})' "$txt" | tr -d [:cntrl:])
#    date[heures]=$(grep -zoP '(\d{1,2})(?= heure)' "$txt" | tr -d [:cntrl:])
#    date[employ]=$(grep -zoP '(?<=SALARIE\n\n).*' "$txt" | tr -d [:cntrl:])
#}

extract_guso_decla () {
    if [ "${DEBUG}" -ne 0 ]
        then
        echo "Généré avec guso"
    fi
    date[debut]=$(grep -zoP '(?:(?<= déduction[\n 0-9]{9})|(?<=H\n\n))([\/ 0-9]+)\n' "$txt" | tr -d '[:space:]'| tr -d [:cntrl:])
    date[fin]=$(grep -zoP '(?<=[0-9 ]{5}\nau )([0-9 ]{3}\n[\/ 0-9]*)\n' "$txt" | tr -d '[:space:]'| tr -d [:cntrl:])
    date[heures]=$(grep -zoP '(?<=la déduction\n\n)([ 0-9]{5})' "$txt" | tr -d '[:space:]' | tr -d [:cntrl:] | sed 's/^0*//')
    date[cachets]=$(grep -zoP '(?<=Nombre de cachets\n\n)([\d\s]{1,5})' "$txt" | tr -d '[:space:]' | tr -d [:cntrl:] | sed 's/^0*//')
    date[employ]=$(grep -zoP '(?<=\n\n)(.*)(?=\n\nwww\.guso\.fr)' "$txt" | tr -d [:cntrl:])
    date[brut]=$(grep -zoP '(?:[ \d]+\/[ \d]+\/[ \d]+)\n\n(?:du\n\n){0,1}([ \d,]{8,})' "$txt" | tail -n 1 | tr -d '[:space:]'| tr -d [:cntrl:])
}

extract_guso24_decla () {
    if [ "${DEBUG}" -ne 0 ]
        then
        echo "Généré avec guso24"
    fi
    date[debut]=$(grep -zoP '(?<=emploi : du ).*' "$txt" | tr -d [:cntrl:])
    date[fin]=$(grep -zoP '(?<=\n\nau ).*' "$txt" | tr -d [:cntrl:])
    date[heures]=$(grep -zoP '(?<=heure\(s\) effectuée\(s\)\n\n)(?!cachet\(s\) ou).*\n\n.*' "$txt" | grep -zoP '\d' | tr -d [:cntrl:])
    if [ -z ${date[heures]} ]; then
		date[heures]=0
		date[estcachet]=true
	else
		date[estcachet]=false
	fi
    date[cachets]=$(grep -zoP '(?<=cachet\(s\) et\/ou\n\n)\d*' "$txt" | tr -d '[:space:]' | tr -d [:cntrl:] | sed 's/^0*//')
    date[employ]=$(grep -zoP '(?<=Raison sociale\n\n).[^\n]*' "$txt" | tr -d [:cntrl:])
    if ! ${date[estcachet]}; then
		date[brut]=$(grep -zoP '(?<=Lieu du spectacle\n\n).*' "$txt" | tr -d '[:space:]'| tr -d [:cntrl:])
	else
		date[brut]=$(grep -zoP '(?<=Salaire brut\n\n).*' "$txt" | tr -d '[:space:]'| tr -d [:cntrl:])
	fi
}

extract_coolpaie () {
    if [ "${DEBUG}" -ne 0 ]
        then
        echo "Généré avec coolpaie"
    fi
    date[debut]=$(grep -zoP '(?<=Contrat en cours\n\n)(\d{2} \d{2} \d{4})' "$txt" | tr -d [:cntrl:] | sed 's/ /\//g')
    date[fin]=$(grep -zoP '(?<=Réalisateur\n\n)(\d{2} \d{2} \d{4})' "$txt" | tr -d [:cntrl:] | sed 's/ /\//g')
    date[heures]=$(grep -zoP '(?<=ET/OU\n\n)(\d{1,2})' "$txt" | tr -d [:cntrl:])
    date[employ]=$(grep -zoP '(?<=Raison sociale ).*' "$txt" | tr -d [:cntrl:])
    date[brut]=$(grep -zoP '(?<=assurance chômage \*\n\n).*' "$txt" | tr -d [:cntrl:])
}

## Logique générale

menage (){
    echo "" > $base/AEMs.csv

}


## Test si la date $1 est superieur a $2
si_annee_preced () {
    [[ $(date -d"$1" +"%s") -gt $(date -d"${2}" +"%s") ]];
}

convert_date () {
    echo "$1" | sed -E "s_([0-9]+)/([0-9]+)/([0-9]+)_\2/\1/\3_"
}

ajout_csv () {
    echo ${date[debut]}";"${date[fin]}";"${date[employ]}";"${date[heures]}"; ${date[cachet]}; "${date[brut]} >> $base/AEMs.csv
 
}

affiche_aem () {
    echo ${date[debut]} ${date[fin]} ${date[employ]} ${date[heures]} ${date[cachets]} ${date[brut]} 

    echo "--------------"
 
}

affiche_stats () {
    echo "Nombre AEM comptées / traitées : " $nbr_aem_compt " / " $nbr_aem_trait
    echo "Non traitées : " $nbr_non_trait
    echo "Nombre d'heures totales sur l'année : " $(($heuresTot + $cachetsTot * "12"))
    echo "- Dont cachets : " $cachetsTot
    if (($heuresTot >= 507))
    then
        echo "Tu as ton statut !"
    else
        echo "Il te manque " $((507 - $heuresTot)) " heures."
    fi
}

comptage_date () {
    nbr_aem_trait=$(($nbr_aem_trait+1))
    
    if si_annee_preced $(convert_date ${date[debut]}) $(convert_date ${debut_statut});
        then
            nbr_aem_compt=$(($nbr_aem_compt+1))
            heuresTot=$((${date[heures]} + $heuresTot))
            if [ -z "${date[cachets]}" ]; then
                date[cachets]=0
            fi
            cachetsTot=$((${date[cachets]} + $cachetsTot))
            
            ajout_csv
            affiche_aem "$txt"
    fi
}


##### Début de la boucle #####

menage
echo "Début, Fin, Employeur, Nbr heures, Nbr cachets, Salaire brut"
echo "------------------------------------------------------------"
for dir in ./*/*/
    do
        cd "${dir}"
        if [ -z "$(ls -A .)" ]; then
            echo "Dossier ${dir} vide"
            echo "--------------"
        else
            for pdf in *.pdf
                do
                    txt=$(basename "${pdf}" .pdf)
                    txt="$txt".txt

                    if [ "${DEBUG}" -ne 0 ]
                        then
                            echo "$txt"
                    fi

                    if [ ! -f "$txt" ] || [ "${DEBUG}" -ne 0 ]
                        then
                        pdftotext "${pdf}"
                    fi
                    if [ "${DEBUG}" -ne 0 ]
                        then
                            echo "$dir$pdf"
                    fi
                    # On appel la fonction implémenté pour le générateur détécté
                    case $(generateurPDF) in
                        spaiectacle) 
                            extract_spaiectacle
                            comptage_date ;;
                        cogilog)
                            extract_cogilog
                            comptage_date ;;
                        coolpaie)
                            extract_coolpaie
                            comptage_date ;;
                        guso_decla)
                            extract_guso_decla
                            comptage_date ;;
                        guso24_decla)
                            extract_guso24_decla
                            comptage_date ;;
                        guso_aem)
                            echo -e "${ERR}/!\\ AEM GUSO non traitée, /!\\"
                            echo -e "/!\\ utiliser les feuillets de déclaration /!\\ ${NC}"
                            echo "--------------"
                            nbr_non_trait=$(($nbr_non_trait+1));;
                        inconnu)
                            echo -e "${ERR}/!\\ Générateur non implémenté /!\\ ${NC}"
                            echo "--------------"
                            nbr_non_trait=$(($nbr_non_trait+1));;

                    esac
                    date=()
    #                rm "$txt"
                done
            fi
        cd $base

    done

affiche_stats
